//
//  DetailController.swift
//  JuntoTask
//
//  Created by Sher Locked on 10.12.2017.
//  Copyright © 2017 Sher Locked. All rights reserved.
//

import UIKit
import Kingfisher

class Detail: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var screenshotImage: UIImageView!
    @IBOutlet weak var upvotesLabel: UILabel!
    
    var product: Product?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureProduct()
        navigationController?.navigationBar.tintColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
    }
    
    func configureProduct() {
        guard let product = product else {
            return
        }
        nameLabel.text = product.name
        descriptionLabel.text = product.description
        upvotesLabel.text = "Votes: \(product.upvotes)"
        let url = URL(string: product.screenshotString)
        screenshotImage.kf.indicatorType = .activity
        screenshotImage.kf.setImage(with: url)
    }

    @IBAction func getItButtonPressed(_ sender: UIButton) {
        
        guard  let product = product else {
            return
        }
        if let url = URL(string: product.redirectString) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
        
    }
}
