//
//  ProductListCell.swift
//  JuntoTask
//
//  Created by Sher Locked on 08.12.2017.
//  Copyright © 2017 Sher Locked. All rights reserved.
//

import UIKit
import Kingfisher

class ProductListCell: UITableViewCell {

    @IBOutlet weak var thumbnailImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var upvotesLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    func configure(with product: Product) {
        nameLabel.text = product.name
        upvotesLabel.text = "Votes: \(product.upvotes)"
        descriptionLabel.text = product.description
        let url = URL(string: product.tnumbnailString)
        thumbnailImage.kf.indicatorType = .activity
        thumbnailImage.kf.setImage(with: url)
        
    }
    
    
}
