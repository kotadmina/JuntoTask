//
//  Product.swift
//  JuntoTask
//
//  Created by Sher Locked on 08.12.2017.
//  Copyright © 2017 Sher Locked. All rights reserved.
//

import Foundation

struct Product {
    var tnumbnailString: String
    var name: String
    var description: String
    var upvotes: Int
    var screenshotString: String
    var redirectString: String
}
