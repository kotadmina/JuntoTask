//
//  ProductList.swift
//  JuntoTask
//
//  Created by Sher Locked on 08.12.2017.
//  Copyright © 2017 Sher Locked. All rights reserved.
//

import UIKit
import AZDropdownMenu

class ProductList: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var refreshControl: UIRefreshControl!
    var dropDownMenu: AZDropdownMenu!
    
    var products: [Product] = []
    var categories: [Category] = []
    var itemsPerPage = 10
    var currentPage = 1
    var isLoading = false
    var currentCategory = Category(name: "Tech", slug: "tech")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTable()
        configureDropDownMenu()
        refresh()
    }
    
    func configureTable() {
        let nib = UINib(nibName: "ProductListCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "ProductListCell")
        tableView.delegate = self
        tableView.dataSource = self
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
        activityIndicator.isHidden = true
    }
    
    func configureDropDownMenu() {
        guard let navigation = navigationController else {
            return
        }
        navigation.navigationBar.isTranslucent = false
        
        let titles = categories.map { category -> String in
            return category.name.uppercased()
        }
        
        dropDownMenu = AZDropdownMenu(titles: titles)
        let titleView = UILabel()
        titleView.frame = navigation.navigationBar.bounds
        titleView.textAlignment = .center
        titleView.text = currentCategory.name.uppercased()
        self.navigationItem.titleView = titleView
        let gesture = UITapGestureRecognizer(target: self, action: #selector(showDropdownMenu))
        titleView.isUserInteractionEnabled = true
        titleView.addGestureRecognizer(gesture)
        
        dropDownMenu.cellTapHandler = { [weak self] (indexPath: IndexPath) -> Void in
            guard let vc = self else {
                return
            }
            let category = vc.categories[indexPath.row]
            (vc.navigationItem.titleView as? UILabel)?.text = category.name.uppercased()
            vc.currentCategory = category
            vc.refresh()
        }
    }
    
    @objc func showDropdownMenu() {
        if self.dropDownMenu.isDescendant(of: self.view) == true {
            self.dropDownMenu.hideMenu()
        } else {
            self.dropDownMenu.showMenuFromView(self.view)
        }
    }
    
    @objc func refresh() {
        isLoading = true
        self.products = []
        tableView.reloadData()
        NetworkService.getProducts(category: currentCategory.slug, page: 1, itemsPerPage: itemsPerPage) { products, error in
            DispatchQueue.main.async {
                self.products = products
                self.tableView.reloadData()
                self.refreshControl.endRefreshing()
                self.currentPage = 1
                self.isLoading = false
                self.handleError(error: error)
            }
        }
    }
    
    func loadMore() {
        isLoading = true
        currentPage += 1
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        NetworkService.getProducts(category: currentCategory.slug, page: currentPage, itemsPerPage: itemsPerPage) { products, error in
            DispatchQueue.main.async {
                self.products.append(contentsOf: products)
                self.tableView.reloadData()
                self.refreshControl.endRefreshing()
                self.isLoading = false
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
                self.handleError(error: error)
            }
        }
    }
    
    func openProduct(product: Product) {
        let storyboard = UIStoryboard(name: "Detail", bundle: nil)
        let detailVC = storyboard.instantiateViewController(withIdentifier: "Detail") as! Detail
        detailVC.product = product
        navigationController?.pushViewController(detailVC, animated: true)
        
    }
    
    func handleError(error: Error?) {
        guard let error = error else {
            return
        }
        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }

}

extension ProductList: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 104
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let cellRow = indexPath.row
        if cellRow == products.count - 1 && !isLoading {
            loadMore()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let product = products[indexPath.row]
        openProduct(product: product)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

extension ProductList: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductListCell") as! ProductListCell
        cell.configure(with: products[indexPath.row])
        return cell
        
    }
    
}
