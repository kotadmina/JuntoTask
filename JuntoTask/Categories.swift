//
//  Categories.swift
//  JuntoTask
//
//  Created by Sher Locked on 11.12.2017.
//  Copyright © 2017 Sher Locked. All rights reserved.
//

import Foundation

struct Category {
    var name: String
    var slug: String
}
