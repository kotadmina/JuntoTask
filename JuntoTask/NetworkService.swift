//
//  NetworkService.swift
//  JuntoTask
//
//  Created by Sher Locked on 08.12.2017.
//  Copyright © 2017 Sher Locked. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class NetworkService {
    //https://api.producthunt.com/v1/posts/all?search[category]=tech&access_token=591f99547f569b05ba7d8777e2e0824eea16c440292cce1f8dfb3952cc9937ff&page=1&per_page=5
    
    private static let serverURL = "https://api.producthunt.com/v1/"
    private static let accessToken = "591f99547f569b05ba7d8777e2e0824eea16c440292cce1f8dfb3952cc9937ff"
    
    static func getCategories(completion: @escaping ([Category], Error?) -> Void) {
        
        let urlString = serverURL + "topics"
        let url = URL(string: urlString)!
        let parameters = ["search[trending]": "true",
                          "per_page": 10,
                          "access_token": accessToken] as [String : Any]
        Alamofire.request(url, method: .get, parameters: parameters).responseData { (response) in
            guard let data = response.data else {
                completion([], nil)
                return
            }
            
            let json = JSON(data)
            let itemsJSON = json["topics"].arrayValue
            var categories: [Category] = []
            for item in itemsJSON {
                let categoryName = item["name"].stringValue
                let categorySlug = item["slug"].stringValue
                let category = Category(name: categoryName, slug: categorySlug)
                categories.append(category)
            }
            
            completion(categories, response.error)
        }
    }
    
    
    static func getProducts(category: String, page: Int, itemsPerPage: Int, completion: @escaping ([Product], Error?) -> Void) {
        
        let urlString = serverURL + "posts/all"
        let url = URL(string: urlString)!
        let parameters = ["search[category]": category,
                          "access_token": accessToken,
                          "page": page,
                          "per_page": itemsPerPage] as [String : Any]
       Alamofire.request(url, method: .get, parameters: parameters).responseData { (response) in
            guard let data = response.data else {
                completion([], nil)
                return
            }
            let json = JSON(data)
            let itemsJSON = json["posts"].arrayValue
            var products: [Product] = []
            for item in itemsJSON {
                let name = item["name"].stringValue
                let description = item["tagline"].stringValue
                let upvotes = item["votes_count"].intValue
                let thumbnailJSON = item["thumbnail"]
                let fullImageURL = thumbnailJSON["image_url"].stringValue
                let imageURL = fullImageURL.components(separatedBy: "?").first ?? ""
                let screenshotJSON = item["screenshot_url"]
                let screenshotURL = screenshotJSON["300px"].stringValue
                let redirectURL = item["redirect_url"].stringValue
                let product = Product(tnumbnailString: imageURL,
                                      name: name,
                                      description: description,
                                      upvotes: upvotes,
                                      screenshotString: screenshotURL,
                                      redirectString: redirectURL)
                products.append(product)
            }
            completion(products, response.error)
        }
        
        
    }
    
}
